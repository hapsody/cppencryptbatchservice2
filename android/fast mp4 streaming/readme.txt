mp4 영상에는 moov 라는 atom 헤더가 들어있는데 비디오 데이터의 인덱스 역할을 합니다.
보통 moov는 파일의 끝에 저장이 되는데 Progressive Download방식의 스트리밍을 하는 경우 moov는 파일의 앞에 위치해야 합니다.

필수적인 영상 정보가 처음 다운로드되고, 바로 영상을 재생할 수 있게 활성화됨을 보장해줍니다.
moov가 영상의 끝에 위치한다면 처음에 강제로 파일 전체를 다운로드하게 됩니다.

# MP4 FastStart.exe (Mp4 FastStart 폴더참조)
 - moov의 위치를 파일의 앞으로 재배치 시켜주는 툴
 - 실행 후 'Open Folder'를 눌러 영상 폴더를 선택합니다. (원본에 덮어쓰니 주의)
 - 'Make FastStart' 버튼을 눌러 moov 위치를 재배치시켜줍니다.

# Mp4Explorer.exe (Mp4Explorer 폴더참조)
 - moov의 헤더가 어느위치에 있는지 확인할 수 있는 툴
 - File -> Open -> 파일선택
 - moov가 제일 위에있는지 확인.

위 2개의 툴을 이용하여 moov의 헤더를 이동시키고 파일을 암호화시키고 테스트를 진행하면 됩니다.
