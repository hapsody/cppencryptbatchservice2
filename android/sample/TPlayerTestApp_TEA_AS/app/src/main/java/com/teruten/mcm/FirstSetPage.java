package com.teruten.mcm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.teruten.common.Define;
import com.teruten.sdcard.SdcardMCMActivity;
import com.teruten.tplayertestapp_tea_as.R;

public class FirstSetPage extends AppCompatActivity {

    private CheckBox mRootingCheckBox;
    private CheckBox mEmulatorCheckBox;
    private CheckBox mHDMICheckBox;
    private CheckBox mMiracastCheckBox;
    private CheckBox mUSBCheckBox;
    private CheckBox mBluetoothCheckBox;
    private CheckBox mDataCheckBox;
    private CheckBox mWiFiCheckBox;
    private CheckBox mProcessCheckBox;
    private CheckBox mRemoteCheckBox;
    private CheckBox mCaptureKeyCheckBox;
    private CheckBox mGpsCheckBox;
    private CheckBox mRecodeCheckBox;
    private CheckBox mAudioRecodeCheckBox;

    private Button mOKBtn;
    private int mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_set_page);

        mRootingCheckBox = (CheckBox)findViewById(R.id.rooting_check);
        mEmulatorCheckBox = (CheckBox)findViewById(R.id.emulator_check);
        mHDMICheckBox = (CheckBox)findViewById(R.id.hdmi_check);
        mMiracastCheckBox = (CheckBox)findViewById(R.id.miracast_check);
        mUSBCheckBox = (CheckBox)findViewById(R.id.usb_check);
        mBluetoothCheckBox = (CheckBox)findViewById(R.id.bluetooth_check);
        mDataCheckBox = (CheckBox)findViewById(R.id.data_check);
        mWiFiCheckBox = (CheckBox)findViewById(R.id.wifi_check);
        mProcessCheckBox = (CheckBox)findViewById(R.id.process_check);
        mRemoteCheckBox = (CheckBox)findViewById(R.id.remote_check);
        mCaptureKeyCheckBox = (CheckBox)findViewById(R.id.shortkey_check);
        mGpsCheckBox = (CheckBox)findViewById(R.id.gps_check);
        mRecodeCheckBox = (CheckBox)findViewById(R.id.recode_check);
        mAudioRecodeCheckBox = (CheckBox)findViewById(R.id.audio_recode_check);
        mOKBtn = (Button)findViewById(R.id.ok_button);
        mOKBtn.setOnClickListener(btnClickListener);

        Intent intent = getIntent();
        mType = intent.getIntExtra("type", -1);
        MCMFlags flags = (MCMFlags)intent.getSerializableExtra("mcmFlags");
        if(flags.isRooting())
            mRootingCheckBox.setChecked(true);
        if(flags.isEmulator())
            mEmulatorCheckBox.setChecked(true);
        if(flags.isHDMI())
            mHDMICheckBox.setChecked(true);
        if(flags.isMiracast())
            mMiracastCheckBox.setChecked(true);
        if(flags.isUSB())
            mUSBCheckBox.setChecked(true);
        if(flags.isBluetooth())
            mBluetoothCheckBox.setChecked(true);
        if(flags.isData())
            mDataCheckBox.setChecked(true);
        if(flags.isWiFi())
            mWiFiCheckBox.setChecked(true);
        if(flags.isProcess())
            mProcessCheckBox.setChecked(true);
        if(flags.isRemote())
            mRemoteCheckBox.setChecked(true);
        if(flags.isCaptureKey())
            mCaptureKeyCheckBox.setChecked(true);
        if(flags.isGps())
            mGpsCheckBox.setChecked(true);
        if(flags.isRecode())
            mRecodeCheckBox.setChecked(true);
        if(flags.isAudioRecode())
            mAudioRecodeCheckBox.setChecked(true);

    }

    Button.OnClickListener btnClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ok_button:
                    MCMFlags flags = new MCMFlags();
                    flags.setRooting(mRootingCheckBox.isChecked());
                    flags.setEmulator(mEmulatorCheckBox.isChecked());
                    flags.setHDMI(mHDMICheckBox.isChecked());
                    flags.setMiracast(mMiracastCheckBox.isChecked());
                    flags.setUSB(mUSBCheckBox.isChecked());
                    flags.setBluetooth(mBluetoothCheckBox.isChecked());
                    flags.setData(mDataCheckBox.isChecked());
                    flags.setWiFi(mWiFiCheckBox.isChecked());
                    flags.setProcess(mProcessCheckBox.isChecked());
                    flags.setRemote(mRemoteCheckBox.isChecked());
                    flags.setCaptureKey(mCaptureKeyCheckBox.isChecked());
                    flags.setGps(mGpsCheckBox.isChecked());
                    flags.setRecode(mRecodeCheckBox.isChecked());
                    flags.setAudioRecode(mAudioRecodeCheckBox.isChecked());

                    Intent intent = null;
                    if(mType == Define.SDCARD_MCM_ACTIVITY_FLAG)
                        intent = new Intent(FirstSetPage.this, SdcardMCMActivity.class);
                    else if(mType == Define.DOWNLOAD_MCM_ACTIVITY_FLAG)
                        //intent = new Intent(FirstSetPage.this, DownloadMCMActivity.class);

                    if(intent == null)
                        finish();

                    intent.putExtra("mcmFlags", flags);
                    setResult(Define.MCM_SETTING_FLAG, intent);
                    finish();
                    break;
            }
        }
    };
}
