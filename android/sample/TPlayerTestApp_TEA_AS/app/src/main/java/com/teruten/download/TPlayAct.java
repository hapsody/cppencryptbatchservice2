package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.teruten.common.DRMINFO;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayer.TPlayer;
import com.teruten.tplayer.view.TPlayerActivity;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.teruten.common.Define.DEVELOPER_MODE;

public class TPlayAct extends TPlayerActivity {

	private static final String TAG = "TPlayerAct";
	private TPlayer _playerControl;					// TPlayer
	TMS4WebServer _webServer;						// DRM

	DRMINFO _info = DRMINFO.getInstance();

	@SuppressLint("DefaultLocale")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String strMediaPath = getIntent().getStringExtra("PATH");
		String strFileName = new File(strMediaPath).getName();
		String strTitle = getIntent().getStringExtra("TITLE");
		Log.e(TAG, "Player, path: " + strMediaPath + ", title: " + strTitle);

		/***************************** DRM ***********************/
		if (strMediaPath.endsWith(".MS4")) {
			int nReturn;

			// 초기화
			if(DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

			// 파일 인증받기 - 원격 파일
			if (strMediaPath.startsWith("http://")) {
				nReturn = _webServer.OpenRemoteMedia(strMediaPath, "nemolabs01");
				if(nReturn != TMS4Encrypt.TMS4E_OK)
				{
					displayMessage("open remote media error : " + nReturn);
					return;
				}
			}
			// 파일 인증받기 - 로컬 파일
			else
			{
				int nTime = (int) (System.currentTimeMillis() / 1000);
				nReturn = _webServer.MediaAccreditation(strMediaPath, nTime);
				if (nReturn != 0) {
					displayMessage("open local media error : " + nReturn);
					Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
					return;
				}
				DRM_showInfo();
			}

			// DRM 용 파일 경로 가져오기.
			strMediaPath = _webServer.getFilePath(strMediaPath);
		}

		/***************************** Player ***********************/

		// 1.player 생성
		_playerControl = new TPlayer(this, TPlayer.LOCALE_KOR);
		_playerControl.SetTPlayerUI(true, true, true);
		BitmapDrawable drawable = (BitmapDrawable)getResources().getDrawable(com.teruten.tplayertestapp_tea_as.R.drawable.teruten);
		_playerControl.SetCI(drawable.getBitmap(), 100);

		String[] arrString = _playerControl.GetBookmarkList(strFileName);
		for(int i = 0; i < arrString.length; i++)
			Log.i(TAG, "BookMark : " + arrString[i]);

		// 2. set Event
		/*
		 * // 플레이 상태 _playerControl.SetOnPlayStatusCb( new TPlayer.OnPlayStatusCb() {
		 *
		 * @Override public void OnPlayStatus( int nStatus, int nDuration, int
		 * nPosition ) {} } );
		 *
		 * // 정한 시간에 callback 으로 position 받기. SetTPlayerTimes 함수로 시간을 설정.
		 * _playerControl.SetOnTimeEventCb( new TPlayer.OnTimeEventCb() {
		 *
		 * @Override public void OnTimeEvent( int nPosition ) { } } );
		 */

		// 북마크 추가시 호출
		_playerControl.SetOnBookmarkAddCb(new TPlayer.OnBookmarkAddCb() {
			@Override
			public void OnBookmarkAdd(int index, int offset, String path) {
				Log.v(TAG, "북마크 추가 : " + index + "\r\n offset : " + offset + "\r\n path : " + path);
			}
		});

		// 북마크 삭제시 호출
		_playerControl.SetOnBookmarkDeleteCb(new TPlayer.OnBookmarkDeleteCb() {

			@Override
			public void OnBookmarkDelete(int index, int offset, String path) {
				Log.v(TAG, "북마크 삭제 : " + index + "\r\n offset : " + offset + "\r\n path : " + path);
			}
		});

		// 이어보기 로 재생할 시 호출
		_playerControl.SetOnResumeVideoCb(new TPlayer.OnResumeVideoCb() {

			public void OnResumeVideo(String strFileName, int nDuration,
									  int nPosition) {
				Log.v(TAG, "이어보기 : " + strFileName + ", duration : " + nDuration + ", position : " + nPosition);
			}
		});

		// 비디오 종료할 때 호출
		_playerControl.SetOnDestroyVideoCb(new TPlayer.OnDestroyVideoCb() {

			public void OnDestroyVideo(String strFileName, int nDuration,
									   int nPosition) {
				Log.v(TAG, "종료 : " + strFileName + ", duration : " + nDuration	+ ", position : " + nPosition);
				//finish();
			}
		});


		if(!_playerControl.PlayMedia(strMediaPath, strTitle, 0))
			displayMessage("error!!!!!!!!!!!!");


	}


	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.e(TAG, "video act onDestroy");
		super.onDestroy();
		if(_webServer != null)
		{
			try {
				_webServer.StopServer();
			} catch (Exception e) {
				e.printStackTrace();
			}
			_webServer = null;
		}


	}

	/**********************************************************
	 * DRM 관련 함수
	 **********************************************************/
	private int DRM_Init() {
		if (_webServer == null) {
			Log.e(TAG, "web server start");
			_webServer = new TMS4WebServer();

			setDeviceData();
			DRMINFO info = DRMINFO.getInstance();

			if(DEVELOPER_MODE) info.MACADDRESS = "90:2b:34:a2:3a:04"; // developer test

			int nReturn = _webServer.InitInstance(info.IMEI, info.MACADDRESS, this);
			if(nReturn != TMS4Encrypt.TMS4E_OK)
			{
				displayMessage("DRM init err : " + nReturn);
				Log.e(TAG, "DRM init err : " + nReturn);
				return nReturn;
			}
			_webServer.StartServer(50000);
		}
		return TMS4Encrypt.TMS4E_OK;
	}

	private void DRM_showInfo() {
		int nEndTime = _webServer.GetEndTime();
		int nCountPolicy = _webServer.GetPolicyCount();
		int nPolicyType = _webServer.GetPolicyType();

		if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD) // 기간 정책이 있는 경우.
		{
			Log.d(TAG, "policy type : period");
			if (nEndTime > 0) {
				long lTime = (long) nEndTime * 1000;
				Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
				Date date = new Date(lTime);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
				displayMessage(format.format(date) + " 까지 재생 가능");
			}
		}
		if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_COUNT) // 횟수 정책이 있는 경우.
		{
			if (nCountPolicy > 0) {
				Log.d(TAG, "policy type : count");
				displayMessage("남은 횟수는  " + nCountPolicy + " 회");
			}
		}
		if (nPolicyType == (TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD |
				TMS4Encrypt.TMS4_POLICY_TYPE_COUNT)) // 기간+횟수 정책이 있는 경우.
		{
			Log.d(TAG, "period + count");
			long lTime = (long) nEndTime * 1000;
			Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
			Date date = new Date(lTime);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
			if ((nEndTime > 0) && (nCountPolicy > 0))
				displayMessage(format.format(date) + " 까지 재생 가능하고 "
						+ nCountPolicy + " 회 재생 가능");
		}
	}


    /**********************************************************
     * 기타 함수
     **********************************************************/
    // Toast 로 메시지 출력
    public void displayMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

	@SuppressLint("HardwareIds")
	private void setDeviceData() {
		TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		@SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

		WifiInfo wifiinfo = null;
		if (wifiManager != null) {
			wifiinfo = wifiManager.getConnectionInfo();
		}

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			return;
		}

		if(tpman == null && wifiinfo == null) return;

		_info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

		if( tpman != null) {
			if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
				_info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
				_info.IMEI = tpman.getDeviceId();
				Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
			}
		}

		if(wifiinfo != null) {
			if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
				_info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
				_info.MACADDRESS = wifiinfo.getMacAddress();

				Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
			}
		}
	}
}
