package com.teruten.mcm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.teruten.tplayertestapp_tea_as.R;

import static com.teruten.common.Define.VIEWLOG;

public class TMCMActivity extends Activity {
	protected TMCM mTMCM = new TMCM();

	private BroadcastReceiver mReceiver = null;
	private final IntentFilter mIntentFilter = new IntentFilter();

	private boolean mRootingFlag;
	private boolean mEmulatorFlag;
	private boolean mHDMIFlag;
	private boolean mMiracastFlag;
	private boolean mUSBFlag;
	private boolean mBluetoothFlag;
	private boolean mDataFlag;
	private boolean mWiFiFlag;
	private boolean mProcessFlag;
	private boolean mRemoteFlag;
	private boolean mCaptureKeyFlag;
	private boolean mGpsFlag;
	private boolean mRecodeFlag;
	private boolean mAudioRecodeFlag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		mRootingFlag = intent.getBooleanExtra("rooting", false);
		mEmulatorFlag = intent.getBooleanExtra("emulator", false);
		mHDMIFlag = intent.getBooleanExtra("hdmi", false);
		mMiracastFlag = intent.getBooleanExtra("miracast", false);
		mUSBFlag = intent.getBooleanExtra("usb", false);
		mBluetoothFlag = intent.getBooleanExtra("bluetooth", false);
		mDataFlag = intent.getBooleanExtra("data", false);
		mWiFiFlag = intent.getBooleanExtra("wifi", false);
		mProcessFlag = intent.getBooleanExtra("process", false);
		mRemoteFlag = intent.getBooleanExtra("remote", false);
		mCaptureKeyFlag = intent.getBooleanExtra("capturekey", false);
		mGpsFlag = intent.getBooleanExtra("gps", false);
		mRecodeFlag = intent.getBooleanExtra("recode", false);
		mAudioRecodeFlag = intent.getBooleanExtra("audiorecode", false);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		TMCMStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		TMCMResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
        TMCMPause();
	}

	@Override
	protected void onDestroy() {
		TMCMEnd();
		super.onDestroy();
	}

	// TMCM Message Broadcast Receiver
	private class TMCMMessageBroadCastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equalsIgnoreCase(TMCM.RECEIVER)) {
				int message = intent.getIntExtra(TMCM.MESSAGE, TMCM.MESSAGE_NONE);
				String detectPackage = intent.getStringExtra(TMCM.PACKAGE_NAME);
				messageProcess(message, detectPackage);
			}
		}
	}

	protected void messageProcess(int msg, String packageName) {
		if(VIEWLOG) Log.e("TMCMSample", "messageProcess msg[" + msg + "]");
	}

	private void TMCMStart() {
		mIntentFilter.addAction(TMCM.RECEIVER);
		if (null == mReceiver) {
			mReceiver = new TMCMMessageBroadCastReceiver();
			registerReceiver(mReceiver, mIntentFilter);
		}

		// (1) init
		if (TMCM.OK_INIT != mTMCM.initProtect(TMCMActivity.this)) {
			Toast.makeText(this, getString(R.string.msg_timeout), Toast.LENGTH_LONG).show();
		}

		if(mTMCM.isTimeOut()) {
			if(VIEWLOG) Log.i("[TERUTEN]", "MCM 기간만료");
			return;
		}
		else {
			if(VIEWLOG) Log.i("[TERUTEN]", "MCM until + " + mTMCM.getTimeLimit());
		}

		// Setting of change logo image path (optional)
		// mTMCM.setLogoImagePath("file://android_asset/protected_screen.png");

        //정책설정
        mTMCM.setAudioVolumeZero(true);

		// (2) set protect
		TMCMProtectSet(); // mTMCM.setProtect()

		// (3) start
		//mTMCM.start(getApplicationContext());
		mTMCM.start(this);

		// Get current illegal protected type (optional)
		// mTMCM.checkProtect();
	}


	private void TMCMProtectSet() {
		if (mRootingFlag) { // Rooting
			mTMCM.setProtect(TMCM.TYPE_ROOTING);
		}
		if (mEmulatorFlag) { // Emulator
			mTMCM.setProtect(TMCM.TYPE_EMULATOR);
		}
		if (mHDMIFlag) { // HDMI
			mTMCM.setProtect(TMCM.TYPE_HDMI);
		}
		if (mMiracastFlag) { // Miracasat
			mTMCM.setProtect(TMCM.TYPE_MIRACAST);
		}
		if (mUSBFlag) { // USB
			mTMCM.setProtect(TMCM.TYPE_USB);
		}
		if (mBluetoothFlag) { // Bluetooth
			mTMCM.setProtect(TMCM.TYPE_BLUETOOTH);
		}
		if (mDataFlag) { // Data
			mTMCM.setProtect(TMCM.TYPE_DATANET);
		}
		if (mWiFiFlag) { // Wi-Fi
			mTMCM.setProtect(TMCM.TYPE_WIFI);
			// optional
			// mTMCM.setAllowPasswordWiFi();
			// mTMCM.setAllowSpecialWiFi(ap_name, ap_macaddress);
		}
		if (mProcessFlag) { // Process Check
			mTMCM.setProtect(TMCM.TYPE_PROCESS);
		}
		if (mRemoteFlag) { // Remote Check
			mTMCM.setProtect(TMCM.TYPE_REMOTE);
		}
		if (mCaptureKeyFlag) { // CaptureKey
			mTMCM.setProtect(TMCM.TYPE_SHORTKEY);
		}
		if (mGpsFlag) { // GPS
			mTMCM.setProtect(TMCM.TYPE_GPS);
		}
		if (mRecodeFlag) { // RECODE
			mTMCM.setProtect(TMCM.TYPE_RECORD);
		}
		if (mAudioRecodeFlag) { // AUDIO_RECODE
			mTMCM.setProtect(TMCM.TYPE_AUDIO_RECODE);
		}
	}

	protected void TMCMResume() {
		mTMCM.resume(this);
	}

	protected void TMCMPause() {
		mTMCM.pause();
	}

	private void TMCMEnd() {
		// (5) final
		if (null != mReceiver) {
			unregisterReceiver(mReceiver);
		}
		mReceiver = null;
		mTMCM.finalProtect();
	}


}