package com.teruten.vr360;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.teruten.common.Define;
import com.teruten.sdcard.SdcardAct;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.BuildConfig;
import com.teruten.tplayertestapp_tea_as.R;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static com.teruten.common.Define.VIEWLOG;

public class Vr360Activity extends AppCompatActivity implements View.OnClickListener,
        ListView.OnItemClickListener,
        ListView.OnItemLongClickListener {

    //태그
    final public static String TAG = "TplayerTest";
    // 저장 경로
    final public static String strDefaultDownloadPath
            = Environment.getExternalStorageDirectory().getAbsolutePath() + "/teruten/";

    // layout
    private Button btnDownload;
    private Button btnListUpdate;

    // listView
    private ListView listView;

    //목록 관련 오브젝트
    private List<String> mFileNames = new ArrayList<String>();
    private ArrayAdapter<String> listViewAdapter;

    //터치하는 동안 중복 터치 막기 위해.
    private boolean m_bTouch = false;

    // 임시 경로
    File downloadDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vr360);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();

            StrictMode.setThreadPolicy(policy);
        }

        // 초기화
        initLayout();

        // 다운로드 폴더가 없으면 생성
        downloadDir = new File(strDefaultDownloadPath);
        if (!downloadDir.exists())
            downloadDir.mkdirs();

        if (!checkPermissions() && Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            updateFileList();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_download:
                Toast.makeText(this, "Download", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn_list_update:
                mFileNames.clear();
                updateFileList();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if(VIEWLOG) Log.d(TAG, "long click.");
        String strPath = strDefaultDownloadPath + parent.getItemAtPosition(position).toString();
        if(VIEWLOG) Log.d(TAG, "path : " + strPath);
        File file = new File(strPath);
        file.delete();
        listViewAdapter.clear();
        updateFileList();
        displayMessage(strPath + "\r\ndelete");
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // 클릭시 영상 재생
        if(!m_bTouch)
        {
            m_bTouch = true;

            String strFilePath = downloadDir.getPath() + "/" + parent.getItemAtPosition(position).toString();
            playMedia(strFilePath);

            m_bTouch = false;
        }
    }

    // 파일 리스트 갱신
    public void updateFileList(){
        try {
            String ext = Environment.getExternalStorageState();
            String path = null;
            if(ext.equals(Environment.MEDIA_MOUNTED)){
                path = downloadDir.getPath();
            } else {
                path = Environment.MEDIA_UNMOUNTED;
                displayMessage("SD카드를 찾을 수 없습니다.");
                return;
            }

            File files = new File(path);
            listViewAdapter = new ArrayAdapter<String>(this, R.layout.file_list_item, mFileNames);

            if(files.exists())
            {
                File[] fileList = files.listFiles();
                if(fileList != null) {
                    for (File file : files.listFiles()) {
                        if (file.isFile())
                            mFileNames.add(file.getName());
                    }
                }
            }

            listView.setAdapter(listViewAdapter);
            listViewAdapter.notifyDataSetChanged();

        } catch (NullPointerException e) {
            displayMessage("다운로드 받은 파일이 없습니다.");
            if(VIEWLOG) Log.e("FileList", e.toString());
        } catch (Exception e){
            displayMessage("Error Occured");
            if(VIEWLOG) Log.e("FileList", e.toString());
        }
    }

    // 미디어 플레이
    private void playMedia(String path) {
        if(VIEWLOG) Log.e(TAG, "play path: " + path);

        Intent intent = new Intent(this, Vr360Act.class);
        intent.putExtra("PATH",path);
        intent.putExtra("TITLE","title");
        startActivity(intent);
    }

    // Toast 로 메시지 출력
    public void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLayout()
    {
        btnDownload = (Button)findViewById(R.id.btn_download);
        btnListUpdate = (Button)findViewById(R.id.btn_list_update);
        btnDownload.setOnClickListener(this);
        btnListUpdate.setOnClickListener(this);

        // 리스트에서 길게 누르면 파일 삭제
        listView = (ListView)findViewById(R.id.lv_listview);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviceRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

        if( shouldProviceRationale ) {
            new AlertDialog.Builder(this)
                    .setTitle("알림")
                    .setMessage("저장소 권한이 필요합니다.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startPermissionRequest();
                        }
                    })
                    .create()
                    .show();
        } else {
            startPermissionRequest();
        }
    }

    private void startPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[] {Manifest.permission.READ_EXTERNAL_STORAGE} , Define.REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case Define.REQUEST_PERMISSIONS_REQUEST_CODE:
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                    updateFileList();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("알림")
                            .setMessage("저장소 권한이 필요합니다. 환경 설정에서 저장소 권한을 허가해주세요.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .create()
                            .show();
                }
        }
    }
}
