package com.teruten.sdcard;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.R;

public class SdcardAct extends Activity {

    VideoView _video = null;
    private TMS4WebServer _webServer;
    String TAG = "VideoDRMAct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);
        _video = (VideoView)findViewById(R.id.video);
        MediaController mc = new MediaController(this);
        _video.setMediaController(mc);

        String strMediaPath = getIntent().getStringExtra("PATH");

        /***************************** DRM ***********************/
        if (strMediaPath.endsWith(".MS4")) {
            int nReturn;

            // 초기화
            if( DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

            // 파일 인증받기 - 원격 파일
            if (strMediaPath.startsWith("http://")) {

            }
            // 파일 인증받기 - 로컬 파일
            else
            {
                nReturn = _webServer.MediaAccreditation(strMediaPath, "ConceptA01");
                if (nReturn != 0) {
                    displayMessage("open local media error : " + nReturn);
                    Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
                    return;
                }
            }

            // DRM 용 파일 경로 가져오기.
            strMediaPath = _webServer.getFilePath(strMediaPath);
        }

        _video.setVideoPath(strMediaPath);
        _video.start();
        Log.e("VideoAct", "media path : " + strMediaPath);

        _video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                Log.e("VideoAct", "completion.");
            }
        });
        _video.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e("VideoAct", "error.");
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "video act onDestroy");
        if(_webServer != null)
        {
            try {
                _webServer.StopServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
            _webServer = null;
        }

        super.onDestroy();
    }


    /**********************************************************
     * DRM 관련 함수
     **********************************************************/
    private int DRM_Init() {
        int nReturn = -1;

        if (_webServer == null) {
            Log.e(TAG, "web server start");
            _webServer = new TMS4WebServer();
            nReturn = _webServer.InitInstance(this, "ConceptA01");
            if(nReturn != TMS4Encrypt.TMS4E_OK)
            {
                displayMessage("DRM init err : " + nReturn);
                Log.e(TAG, "DRM init err : " + nReturn);
                return nReturn;
            }
            _webServer.StartServer(50000);
        }
        return nReturn;
    }

    /**********************************************************
     * 기타 함수
     **********************************************************/
    // Toast 로 메시지 출력
    public void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
