package com.teruten.vr360;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.R;
import com.teruten.vr360.player.SphericalVideoPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.teruten.common.Define.VIEWLOG;

/**
 * Created by KIM on 2018-01-29.
 */

public class Vr360Act extends AppCompatActivity {
    private SphericalVideoPlayer videoPlayer;
    private TMS4WebServer _webServer;
    String TAG = "VideoDRMAct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_vr360);
        videoPlayer = (SphericalVideoPlayer) findViewById(R.id.spherical_video_player);

        String strMediaPath = getIntent().getStringExtra("PATH");

        /***************************** DRM ***********************/
        if (strMediaPath.endsWith(".MS4")) {
            int nReturn;

            // 초기화
            if( DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

            // 파일 인증받기 - 원격 파일
            if (strMediaPath.startsWith("http://")) {

            }
            // 파일 인증받기 - 로컬 파일
            else
            {
                nReturn = _webServer.MediaAccreditation(strMediaPath);
                if (nReturn != 0) {
                    displayMessage("open local media error : " + nReturn);
                    if(VIEWLOG) Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
                    return;
                }
            }

            // DRM 용 파일 경로 가져오기.
            strMediaPath = _webServer.getFilePath(strMediaPath);
        }

        videoPlayer.setVideoURIPath(strMediaPath);
        videoPlayer.playWhenReady();
        if(VIEWLOG) Log.e("VideoAct", "media path : " + strMediaPath);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(VIEWLOG) Log.e(TAG, "video act onDestroy");
        if(_webServer != null)
        {
            try {
                _webServer.StopServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
            _webServer = null;
        }

        super.onDestroy();
    }


    /**********************************************************
     * DRM 관련 함수
     **********************************************************/
    private int DRM_Init() {
        int nReturn = -1;

        if (_webServer == null) {
            if(VIEWLOG) Log.e(TAG, "web server start");
            _webServer = new TMS4WebServer();
            nReturn = _webServer.InitInstance(this, "InnoSimA01");
            if(nReturn != TMS4Encrypt.TMS4E_OK)
            {
                displayMessage("DRM init err : " + nReturn);
                if(VIEWLOG) Log.e(TAG, "DRM init err : " + nReturn);
                return nReturn;
            }
            _webServer.StartServer(50000);
        }
        return nReturn;
    }

    /**********************************************************
     * 기타 함수
     **********************************************************/
    // Toast 로 메시지 출력
    public void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**********************************************************
     * Video 초기화 관련
     **********************************************************/
    private void init() {
        videoPlayer.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                videoPlayer.initRenderThread(surface, width, height);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                videoPlayer.releaseResources();
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        });
        videoPlayer.setVisibility(View.VISIBLE);
    }

    public static String readRawTextFile(Context context, int resId) {
        InputStream is = context.getResources().openRawResource(resId);
        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader buf = new BufferedReader(reader);
        StringBuilder text = new StringBuilder();
        try {
            String line;
            while ((line = buf.readLine()) != null) {
                text.append(line).append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }
}
