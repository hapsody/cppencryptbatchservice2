package com.teruten.common;

/**
 * Created by KIM on 2016-11-16.
 */

public class DRMINFO {
    public int HINT = 0;
    public String IMEI = "";
    public String MACADDRESS = "";
    public String USERID = "";
    public String URL = "";
    public String WATERMARK = "";
    public String UUID = "";
    public String SELLERID = "";
    public String SELLERCONTENTID = "";
    public int COMPANYCODE = 0;

    private static DRMINFO instance = new DRMINFO();


    public static DRMINFO getInstance() {
        return instance;
    }
}
