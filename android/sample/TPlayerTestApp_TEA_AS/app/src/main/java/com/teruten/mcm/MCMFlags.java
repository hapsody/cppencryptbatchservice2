package com.teruten.mcm;

import java.io.Serializable;

/**
 * Created by KIM on 2017-10-27.
 */

public class MCMFlags implements Serializable {

    private static final long serialVersionUID = 1174538249679227708L;

    private boolean mRooting;
    private boolean mEmulator;
    private boolean mHDMI;
    private boolean mMiracast;
    private boolean mUSB;
    private boolean mBluetooth;
    private boolean mData;
    private boolean mWiFi;
    private boolean mProcess;
    private boolean mRemote;
    private boolean mCaptureKey;
    private boolean mGps;
    private boolean mRecode;
    private boolean mAudioRecode;

    public boolean isRooting() {
        return mRooting;
    }

    public void setRooting(boolean mRooting) {
        this.mRooting = mRooting;
    }

    public boolean isEmulator() {
        return mEmulator;
    }

    public void setEmulator(boolean mEmulator) {
        this.mEmulator = mEmulator;
    }

    public boolean isHDMI() {
        return mHDMI;
    }

    public void setHDMI(boolean mHDMI) {
        this.mHDMI = mHDMI;
    }

    public boolean isMiracast() {
        return mMiracast;
    }

    public void setMiracast(boolean mMiracast) {
        this.mMiracast = mMiracast;
    }

    public boolean isUSB() {
        return mUSB;
    }

    public void setUSB(boolean mUSB) {
        this.mUSB = mUSB;
    }

    public boolean isBluetooth() {
        return mBluetooth;
    }

    public void setBluetooth(boolean mBluetooth) {
        this.mBluetooth = mBluetooth;
    }

    public boolean isData() {
        return mData;
    }

    public void setData(boolean mData) {
        this.mData = mData;
    }

    public boolean isWiFi() {
        return mWiFi;
    }

    public void setWiFi(boolean mWiFi) {
        this.mWiFi = mWiFi;
    }

    public boolean isProcess() {
        return mProcess;
    }

    public void setProcess(boolean mProcess) {
        this.mProcess = mProcess;
    }

    public boolean isRemote() {
        return mRemote;
    }

    public void setRemote(boolean mRemote) {
        this.mRemote = mRemote;
    }

    public boolean isCaptureKey() {
        return mCaptureKey;
    }

    public void setCaptureKey(boolean mCaptureKey) {
        this.mCaptureKey = mCaptureKey;
    }

    public boolean isGps() {
        return mGps;
    }

    public void setGps(boolean mGps) {
        this.mGps = mGps;
    }

    public boolean isRecode() {
        return mRecode;
    }

    public void setRecode(boolean mRecode) {
        this.mRecode = mRecode;
    }

    public boolean isAudioRecode() {
        return mAudioRecode;
    }

    public void setAudioRecode(boolean mAudioRecode) {
        this.mAudioRecode = mAudioRecode;
    }
}
