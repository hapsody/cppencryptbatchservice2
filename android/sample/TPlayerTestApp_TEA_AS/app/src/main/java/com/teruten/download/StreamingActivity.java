package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.teruten.common.DRMINFO;
import com.teruten.common.Define;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tplayertestapp_tea_as.BuildConfig;
import com.teruten.tplayertestapp_tea_as.R;

public class StreamingActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    //태그
    final public static String TAG = "TplayerTest";

    // TPlayer
    private boolean _useTPlayer = false;
    private boolean _useExoPlayer = false;

    // 다운로드 경로
    private String  originalNonStreamingPath
            = "http://122.199.199.140/files/Streaming/Tangled.mp4";

    private String  originalStreamingPath
            = "http://122.199.199.140/files/Streaming/no_subtitle1.mp4";

    private String ms4NonStreamingPath
            = "http://122.199.199.140/files/Streaming/H_step1_ext_05_streaming_s01.mp4.MS4";

    private String  ms4StreamingPath
            = "http://122.199.199.140/files/Streaming/nemolab_0xnemolab01_2.mp4.MS4";

    private String ms4StreamingMoovPath
            = "http://122.199.199.140/files/Streaming/nemolab_0xnemolab01_moov.mp4.MS4";

    DRMINFO _info = DRMINFO.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();

            StrictMode.setThreadPolicy(policy);
        }

        if (!checkPermissions() && Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            // 초기화
            initLayout();
            setDeviceData();
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch(checkedId) {
            case R.id.rdbTPlayer:
                _useTPlayer = true;
                _useExoPlayer = false;
                break;

            case R.id.rdbDefaultVideo:
                _useTPlayer = false;
                _useExoPlayer = false;
                break;

            case R.id.rdbExoplayer:
                _useExoPlayer = true;
                _useTPlayer = false;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_mp4_non_streaming:
                playMedia(originalNonStreamingPath);
                break;

            case R.id.btn_ms4_non_streaming:
                playMedia(ms4NonStreamingPath);
                break;

            case R.id.btn_mp4_streaming:
                playMedia(originalStreamingPath);
                break;

            case R.id.btn_ms4_streaming:
                playMedia(ms4StreamingPath);
                break;

            case R.id.btn_ms4_streaming_moov:
                playMedia(ms4StreamingMoovPath);
                break;
        }
    }

    // 미디어 플레이
    private void playMedia(String path) {
        Log.e(TAG, "play path: " + path);

        Intent intent = null;

        if(_useTPlayer)
            intent = new Intent(this, TPlayAct.class);
        else if(_useExoPlayer) {
            intent = new Intent(this, ExoPlayerAct.class);
            intent.putExtra("drmType", Define.STREAMING);
        }
        else
            intent = new Intent(this, VideoAct.class);

        intent.putExtra("PATH",path);
        intent.putExtra("TITLE","title");
        startActivity(intent);
    }

    // Toast 로 메시지 출력
    private void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HardwareIds")
    private void setDeviceData() {
        TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiinfo = null;
        if (wifiManager != null) {
            wifiinfo = wifiManager.getConnectionInfo();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(tpman == null && wifiinfo == null) return;

        _info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

        if( tpman != null) {
            if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
                _info.IMEI = tpman.getDeviceId();
                Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
            }
        }

        if(wifiinfo != null) {
            if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
                _info.MACADDRESS = wifiinfo.getMacAddress();
                Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
            }
        }
    }

    private void initLayout() {
        Button btnOriginalNonStreaming = (Button) findViewById(R.id.btn_mp4_non_streaming);
        Button btnMs4NonStreaming = (Button) findViewById(R.id.btn_ms4_non_streaming);
        Button btnOriginalStreaming = (Button) findViewById(R.id.btn_mp4_streaming);
        Button btnMs4Streaming = (Button) findViewById(R.id.btn_ms4_streaming);
        Button btnMs4StreamingMoov = (Button) findViewById(R.id.btn_ms4_streaming_moov);
        RadioGroup rdG = (RadioGroup) findViewById(R.id.rd_radio_group);

        btnOriginalNonStreaming.setOnClickListener(this);
        btnMs4NonStreaming.setOnClickListener(this);
        btnOriginalStreaming.setOnClickListener(this);
        btnMs4Streaming.setOnClickListener(this);
        btnMs4StreamingMoov.setOnClickListener(this);
        rdG.setOnCheckedChangeListener(this);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviceRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE);

        if( shouldProviceRationale ) {
            new AlertDialog.Builder(this)
                    .setTitle("알림")
                    .setMessage("단말기 정보 획득 권한이 필요합니다.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startPermissionRequest();
                        }
                    })
                    .create()
                    .show();
        } else {
            startPermissionRequest();
        }
    }

    private void startPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[] {Manifest.permission.READ_PHONE_STATE} , Define.REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case Define.REQUEST_PERMISSIONS_REQUEST_CODE:
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                    // 초기화
                    initLayout();
                    setDeviceData();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("알림")
                            .setMessage("단말기 정보 획득 권한이 필요합니다. 환경 설정에서 저장소 권한을 허가해주세요.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .create()
                            .show();

                    // 초기화
                    initLayout();
                    setDeviceData();
                }
        }
    }
}
