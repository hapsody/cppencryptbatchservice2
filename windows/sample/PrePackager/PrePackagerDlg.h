
// PrePackagerDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "Wininet.h"    
#undef GetMessage          // workaround for AWSError method GetMessage(), and ATL conflict
#undef GetObject           // workaround for Aws::S3::S3Client::GetObject

#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/CreateBucketRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/Permission.h>
#include <aws/s3/model/GetObjectAclRequest.h>

#include <aws/core/utils/memory/stl/AwsStringStream.h> 

#include <aws/core/Core_EXPORTS.h>

#include <aws/core/utils/logging/FormattedLogSystem.h>

#include <aws/transfer/TransferManager.h>

#include <aws/core/client/ClientConfiguration.h>
#include <aws/core/http/Scheme.h>
#include <aws/core/utils/threading/Executor.h>

#include <iostream>
#include <fstream>

extern "C" {
#include <curl/curl.h>
}

#define READ_BUF_SIZE    1024

#define UM_COPYCOMPLETE	(WM_USER + 458)
#define UM_MSGMSG		(WM_USER + 459)
#define UM_PRGRSUPDATE	(WM_USER + 460)
#define UM_PRGRSSET		(WM_USER + 461)

typedef struct _tagCON_INFO
{
	TCHAR tzOrgContentPath[MAX_PATH];
	TCHAR tzOrgContentName[MAX_PATH];
	TCHAR tzOrgRemainderPath[MAX_PATH];
	TCHAR tzDstContentPath[MAX_PATH];
	BOOL bDir;
	ULONGLONG n64FileSize;
	HWND hUiHandle;
	LPBOOL pbCancel;
} CON_INFO, *LPCON_INFO;

typedef struct _tagRATIO_INFO
{
	ULONGLONG nCopiedSize;
	ULONGLONG nFullSize;
} RATIO_INFO, *LPRATIO_INFO;

typedef struct _tagENCTYPE_INFO
{
	INT nEncType;
}ENCTYPE_INFO, *LPENCTYPE_INFO;

typedef struct _tagCURCON_INFO
{
	LPCON_INFO pConInfo;
	LPRATIO_INFO pRatioInfo;
	ENCTYPE_INFO sEncTypeInfo;
} CURCURCON_INFO, *LPCURCURCON_INFO;

typedef struct _tagHANDLE_SET
{
	HANDLE hCopyThread;
	HWND hUiHandle;
} HANDLE_SET, *LPHANDLE_SET;

// CPrePackagerDlg 대화 상자
class CPrePackagerDlg : public CDialog
{
// 생성입니다.
public:
	CPrePackagerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PREPACKAGER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	~CPrePackagerDlg() // 소멸자
	{
		//CString deletePath = _T("C:\\Users\\") + m_loginAccount + _T("\\.aws\\credentials");
		//::DeleteFile(deletePath);
		m_logOut.close();
	}
	CListBox m_ctrLogListBox;
	afx_msg void OnBnClickedOriginselButton();
	afx_msg void OnBnClickedEncselButton();
	afx_msg LRESULT OnCopyPrgrs(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCopyComplete(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCopyMsgMsg(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnPrgrsSet(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPrgrsUpdate(WPARAM wParam, LPARAM lParam);

	CString m_valOriginFolderPath;
	CString m_valEncFolderPath;
	BOOL m_bEncryptCancel;
	CProgressCtrl m_ctrEncryptProgressBar;
	int EncryptProcess(void);
	int EncFileListClear(void);
	int OriginToEnc(void);
	int LoadMS4XLib(void);
	void EncFileListing(CString strDirPath, int baseDirLenth);
	void EncOneFileListing();
	HMODULE m_hTMS4Xlib;
	HANDLE_SET m_TwoHandleSet;
	CPtrArray ArrDnFilesEntry;
	ULONGLONG m_n64OrgConSize;
	afx_msg void OnDestroy();
	int m_valEncUnitSelRadio;
	afx_msg void OnBnClickedFileselRadio();
	afx_msg void OnBnClickedFolderselRadio();
	
	int m_valEncType;
	int m_valEncKeyTypeRadio;
	afx_msg void OnBtnClickedMacRadio();
	afx_msg void OnBtnClickedStreamingRadio();
	ULONGLONG GetFileSize(TCHAR* tzFilePath);

	
	int getFileFromHttp(LPCWCHAR pszUrl, char * pszFile);
	void ErrorExit(LPTSTR lpszFunction);
	afx_msg void OnBnClickedPeriodicRun();

	void UploadProgressHandler(const Aws::Transfer::TransferManager*, const std::shared_ptr<const Aws::Transfer::TransferHandle>&transferHandle);

	static BOOL gotoXY(int x, int y);
	static COORD getXY();
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedFetchlistButton();

	void MultiUpload();
	

	
	void notifyToServer();

	int iSaleFileNo;
	std::mutex mtx_lock;
	std::string m_strTargetPath;
	std::string m_origFileNameString;
	CString m_origFileName;
	
	CURL *m_curl;
	CURLcode m_curlRes;

	static UINT downloadThreadFunc(LPVOID _mothod);
	static UINT multiDownloadThreadFunc(LPVOID _mothod);
	void CPrePackagerDlg::disableAllButtons();
	void CPrePackagerDlg::enableAllButtons();


	HANDLE m_hBatchJobThread = NULL;
	enum RunSignal {
		//STAT_[현재 상태]_[다음 예정상태]
		STAT_RUN_STOP,
		STAT_RUN_RUN,
		STAT_STOP_RUN,
		STAT_STOP_STOP,
		STAT_RUN_WAIT
	};
	RunSignal runSignal = STAT_STOP_STOP;

	static const std::string currentDateTime();
	void Logger(std::string str);
	std::ofstream m_logOut;
	static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
	
	CComboBox m_cbFetchAddress;
	CComboBox m_cbNotiAddress;
	
	int m_valModeType;
	int m_valModeTypeRadio;
	afx_msg void OnBnClickedTestmodeRadio();
	afx_msg void OnBnClickedLivemodeRadio();
	
	CString m_loginAccount;
};

