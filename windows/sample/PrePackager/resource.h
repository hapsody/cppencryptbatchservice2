//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// PrePackager.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PREPACKAGER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_ORIGINFOLDER_EDIT           1000
#define IDC_ENCFOLDER_EDIT              1001
#define IDC_ORIGINSEL_BUTTON            1002
#define IDC_ENCSEL_BUTTON               1003
#define IDC_ENCSTART_BUTTON             1004
#define IDC_ENCEND_BUTTON               1005
#define IDC_ENCSTATS_PROGRESS           1006
#define IDC_LIST2                       1008
#define IDC_ENCLOG_LIST                 1008
#define IDC_FILESEL_RADIO               1009
#define IDC_FOLDERSEL_RADIO             1010
#define IDC_KEYTYPE_MAC                 1011
#define IDC_KEYTYPE_STREAMING           1012
#define IDC_EDIT1                       1013
#define IDC_KEY_STREAMING               1013
#define IDC_DOWNLOAD                    1014
#define IDC_DOWNLOAD_BUTTON             1014
#define IDC_UPLOAD_BUTTON               1015
#define IDC_MULTIUPLOAD_BUTTON          1016
#define IDC_COGNITO_RUN                 1017
#define IDC_FETCHLIST_BUTTON            1018
#define IDC_BUTTON1                     1019
#define IDC_MULTIDOWNLOAD_BUTTON        1019
#define IDC_FETCHLIST_ADDRESS           1021
#define IDC_NOTISERVER_ADDRESS          1022
#define IDC_LIVEMODE_RADIO              1024
#define IDC_TESTMODE_RADIO              1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
